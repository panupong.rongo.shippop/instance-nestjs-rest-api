# Instant-Nestjs-API (Nestjs API กึ่งสำเร็จรูป)

> เป็นโปรเจคที่ฉีกซอง (Flok) ไปแล้ว Dev ได้เลยโดย คน dev สนใจแต่ Logic การเขียนโปรแกรมอย่างเดียว

สิ่งที่จะถูก Config ไว้ใน Project

- [Exception filters](https://docs.nestjs.com/exception-filters): เอาไว้ตั้งค่าการ Thow Error Exception ให้เป็น Pattern และ Aspect ได้
- [Pipes](https://docs.nestjs.com/pipes): เอาไว้ validate Input ช่องทางต่างๆของ API และ tranformation เพื่อส่งต่อให้ controller
- [Interceptors](https://docs.nestjs.com/interceptors#response-mapping): transfrom data ก่อน return to client
- [Guards](https://docs.nestjs.com/guards) protect route ด้วย
  - [jwt](https://www.passportjs.org/packages/passport-jwt/) สำหรับ front-end clinet
  - [bearer](http://www.passportjs.org/packages/passport-http-bearer/) สำหระบ back-end client
- [Prisma](https://www.prisma.io/docs/): ORM ที่ใช้ต่อ Database ที่ลองกับ REST API, GraphQL API or grpc API
- docker & docker-compose: ตั้งค่า docker enviroment ให้พร้อม develope
- [Unit Test](https://jestjs.io/) ตั้งค่าและทดสอบ logic เบื้อต้นเพื่อสร้าง Reliable Sofrware :)

---

## Todo Lists

- [✓] Exception Filters
- [✓] Pipes
- [-] Interceptors
- [-] Guards
  - [-] jwt
  - [-] bearer
- [-] Prisma
- [-] docker & docker-compose
- [-] Unit Test

---

## Exception Filters

> เป็น Layer ที่ไว้ใช้จัดการ exception ใน Code เพื่อให้การเขียน Code เป็นระเบียบ และมีีรูปแบบ Pattern ชัดเจน

### example การโยน Error ใน Code

#### Internal Error

ตัวอย่าง Response

```json
{
  "statusCode": 500, // // / http status code
  "errCode": "E-500", // // / error code maybe value equal http status
  "errMessage": "Internal Server Error" // // / error message
}
```

#### Not Accepable

> เคสจัดการ Input ของ Rest API

```json
{
  "statusCode": 406, // // / http status code
  "errCode": "E-1", // // / error code spec case not acceptable code
  "errMessage": "Not Acceptable", // // / error message
  "errorDetails": [
    {
      "type": "param", // / type: param | body
      "property": "id", // / param meter name
      "errorCode": "MUST_BE_A_NUMBER" // / error code
    }
  ] // //  array of Error Input
}
```

### Custom Error

> เคสจัดการ Error อื่นๆ ใน Logic

```json
{
  "statusCode": 500, // // / http status code
  "errCode": "E-999", // // / error code spec case not acceptable code
  "errMessage": "Custom Error Naja" // // / error message
}
```

## Pipes

> เป็น Layer ที่ใช้ ตรวจสอบ (Validate) และ แปลง (Transform) ข้อมูล HTTP Input

ตรวจสอบ code ได้ที่ root/src/validate.pipe.ts

เครื่องมือที่ใช้ในการ validate => [class-validator](https://github.com/typestack/class-validator)

#### ตัวอย่าง Validate Param

```ts
import { IsNotEmpty, IsNumberString } from 'class-validator'

export class FindById {
  @IsNotEmpty()
  @IsNumberString(
    {},
    {
      message: 'id must be a number',
    },
  )
  id: number
}
```

#### ตัวอย่าง Validate Body

```ts
import {
  IsString,
  IsInt,
  IsEmail,
  IsBoolean,
  IsNotEmpty,
} from 'class-validator'

export class ExampleBody1 {
  @IsEmail()
  email: string

  @IsInt()
  age: number

  @IsString()
  name: string

  @IsNotEmpty()
  @IsBoolean()
  haveCar: boolean
}
```
