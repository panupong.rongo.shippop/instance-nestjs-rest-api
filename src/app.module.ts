import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { APP_FILTER, APP_PIPE, APP_INTERCEPTOR } from '@nestjs/core'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { ExamplesModule } from './examples/examples.module'
import configuration from './config/configuration'
import ConfigutaionInterface from './config/configuration.interface'
import { AllExceptionsFilter } from './exeception/all-execption.fillter'
import { ValidationPipe } from './validate.pipe'
import { TransformInterceptor } from './transform.interceptor'
import { AuthModule } from './auth/auth.module'
import { UsersModule } from './users/users.module'

const conf: ConfigutaionInterface = configuration()

const experimentModules = []

if (conf.nodeEnv !== 'production') {
  experimentModules.push(ExamplesModule)
}
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env'], // ตั้งค่าตัวแปร env file path ที่จะอ่าน
      load: [configuration],
      isGlobal: true,
    }),
    ...experimentModules,
    AuthModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },

    AppService,
  ],
})
export class AppModule {}
