import {
  IsString,
  IsInt,
  IsEmail,
  IsBoolean,
  IsNotEmpty,
} from 'class-validator'

export class ExampleBody1 {
  @IsEmail()
  email: string

  @IsInt()
  age: number

  @IsString()
  name: string

  @IsNotEmpty()
  @IsBoolean()
  haveCar: boolean
}
