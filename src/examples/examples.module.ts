import { Module } from '@nestjs/common'
import { ExamplesService } from './examples.service'
import { ExamplesController } from './examples.controller'
import { AuthModule } from '../auth/auth.module'

@Module({
  controllers: [ExamplesController],
  providers: [ExamplesService],
  imports: [AuthModule],
})
export class ExamplesModule {}
