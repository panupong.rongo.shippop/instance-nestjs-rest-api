import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  UseFilters,
  Body,
  Request,
  UseGuards,
} from '@nestjs/common'
// import { AuthGuard } from '@nestjs/passport'
import { LocalAuthGuard } from 'src/auth/local-auth.guard'
import { ExamplesService } from './examples.service'
import { HttpExceptionFilter } from '../exeception/http-exception.filter'
import { ExampleBody1 } from './dto'
import { FindById } from '../common/dto'
import { AuthService } from '../auth/auth.service'
import { JwtAuthGuard } from '../auth/jwt-auth.guard'
import { BearerAuthGuard } from '../auth/bearer-auth.guard'

@Controller('examples')
export class ExamplesController {
  constructor(
    private readonly examplesService: ExamplesService,
    private readonly authService: AuthService,
  ) {}

  // // -------------------------- Exception Filters -------------------
  /**
   * Throwing standard exceptions
   * * https://docs.nestjs.com/exception-filters#throwing-standard-exceptions
   */
  @Get('ef/standard')
  async standardThrowException() {
    throw new HttpException('Internal Server', HttpStatus.INTERNAL_SERVER_ERROR)
  }

  /**
   * Throwing standard exceptions with custom message
   */
  @Get('ef/standard-custom-msg')
  async customThrowException() {
    throw new HttpException(
      {
        errCode: 'E-999',
        errMessage: 'Custom Error Naja',
      },
      HttpStatus.INTERNAL_SERVER_ERROR,
    )
  }
  /**
   * Try to using custom example exeoption filter
   * * https://docs.nestjs.com/exception-filters#exception-filters-1
   */

  @Get('ef/bind-custom-filter')
  @UseFilters(HttpExceptionFilter)
  async bindCustomExeptionFilter() {
    throw new HttpException(
      {
        // status: HttpStatus.FORBIDDEN,
        // error: 'This is a custom message',
        errorKey: 'EXAMPLE_ERROR',
      },
      HttpStatus.FORBIDDEN,
    )
  }

  // // --------------------------------------------------------------
  // // -------------------------- Pipes -----------------------------
  /**
   * Binding pipes#
   * * https://docs.nestjs.com/pipes#binding-pipes
   */
  @Get('pipe/:id1')
  pipeParams1(@Param('id1', ParseIntPipe) id1: number): number {
    return id1
  }

  @Get('pipe-custom/:id1')
  pipeParams2(
    @Param(
      'id1',
      new ParseIntPipe({
        errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE,
      }),
    )
    id1: number,
  ) {
    return id1
  }

  @Get('pipe-class-validation-param/:id')
  pipeClassValidation(@Param() { id }: FindById): number {
    return id
  }

  @Post('pipe-class-validate-1')
  pipePostBody1(@Body() exampleBody1: ExampleBody1): ExampleBody1 {
    return exampleBody1
  }

  // // --------------------------------------------------------------
  // // ------------------ Interceptors -------------------------------
  @Get('interceptor-1')
  testInterceptor() {
    return { title: 1, desc: 'desc' }
  }

  // // ----------------------------------------------------------------
  // // ------------------ Guards -------------------------------------
  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user)
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user
  }

  @UseGuards(BearerAuthGuard)
  @Get('profile-bearer')
  getProfileWithBearerToken(@Request() req) {
    return req.user
  }

  // // ---------------------------------------------------------------
}
