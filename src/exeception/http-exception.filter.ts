import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common'
import {
  // Request,
  Response,
} from 'express'

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp()
    const response = ctx.getResponse<Response>()
    // const request = ctx.getRequest<Request>()
    const status = exception.getStatus()

    let errCode = ''
    let errMessage = ''
    if (status === 500) {
      errCode = 'E-0'
      errMessage = 'Internal Server Error'
    } else {
      errCode = 'E-1'
      errMessage = 'another error'
    }

    // console.info('ctx', ctx)
    // console.info('response', response)
    console.info(`call from HttpExceptionFilter`)

    response.status(status).json({
      errCode,
      errMessage,
      // statusCode: status,
      // timestamp: new Date().toISOString(),
      // path: request.url,
    })
  }
}
