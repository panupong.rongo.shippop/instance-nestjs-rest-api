import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common'
import { HttpAdapterHost } from '@nestjs/core'
import { STATUS_CODES } from 'http'

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: unknown, host: ArgumentsHost): void {
    // In certain situations `httpAdapter` might not be available in the
    // constructor method, thus we should resolve it here.
    const { httpAdapter } = this.httpAdapterHost

    const ctx = host.switchToHttp()

    const httpStatus =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR

    const defaultDetails = {
      errCode: `E-${httpStatus}`,
      errMessage: STATUS_CODES[httpStatus],
    }

    let errDetails = null

    /// / กรณี ที่เป็น exception instanceof HttpException มีโอกาส
    /// / errCode
    /// / errMessage

    if (exception instanceof HttpException) {
      const tmp = exception.getResponse()
      errDetails = typeof tmp === 'string' ? null : tmp
    }

    const responseBody: {
      statusCode: number
      errCode: string
      errMessage: string
      errDetails: any[]
    } = {
      statusCode: httpStatus,
      ...defaultDetails,
      ...errDetails,
    }

    httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus)
  }
}
