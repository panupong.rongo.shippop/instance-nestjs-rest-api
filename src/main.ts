import { NestFactory } from '@nestjs/core'
import { ConfigService } from '@nestjs/config'
// import { ValidationPipe } from '@nestjs/common'
import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)

  // app.useGlobalPipes(
  //   new ValidationPipe({
  //     transform: true,
  //   }),
  // )

  const configService = app.get(ConfigService)
  const port = configService.get<number>('port')
  const preBuildEnv = configService.get<string>('preBuildEnv')

  console.info(`app running @ port: `, port)
  console.info(`test binding data from prebuild: `, preBuildEnv)

  await app.listen(port || 3000)
}
bootstrap()
