import { IsNotEmpty, IsNumberString } from 'class-validator'

export class FindById {
  @IsNotEmpty()
  @IsNumberString(
    {},
    {
      message: 'id must be a number',
    },
  )
  id: number
}
