import { Injectable } from '@nestjs/common'

// This should be a real class/interface representing a user entity
export type User = any

@Injectable()
export class UsersService {
  private readonly users = [
    {
      id: 1,
      email: 'admin@domain.com',
      password: 'admin123456',
      bearerToken: 'B2C9861977D645A36182AB31E9AA7',
      role: ['admin'],
    },
    {
      id: 2,
      email: 'john@domain.com',
      password: 'john123456',
      bearerToken: '45C78975EAFE2D2BFA864BCE7281C',
      role: ['user'],
    },
    {
      id: 3,
      email: 'jane@domain.com',
      password: 'jane123456',
      bearerToken: '9CC1E46AE5EFDC4D3AFDB391A234A',
      role: ['user'],
    },
  ]

  async findOneByEmail(email: string): Promise<User | undefined> {
    return this.users.find((user) => user.email === email)
  }

  async findOneByBearerToken(bearerToken: string): Promise<User | undefined> {
    return this.users.find((user) => user.bearerToken === bearerToken)
  }
}
