import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  Paramtype,
  HttpException,
  HttpStatus,
} from '@nestjs/common'
import { validate, ValidationError } from 'class-validator'
import { plainToClass } from 'class-transformer'

// HttpStatus.NOT_ACCEPTABLE
@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  private readonly FUNCTUIN_TYPE: Function[] = [
    String,
    Boolean,
    Number,
    Array,
    Object,
  ]

  async transform(value: any, { type, metatype, data }: ArgumentMetadata) {
    console.info('type, metatype, data', metatype, type, data)
    // console.info(this.toValidate(metatype))

    if (!metatype || !this.toValidate(metatype)) {
      return value
    }

    const object = plainToClass(metatype, value)
    const errors = await validate(object, {
      transform: true,
      stopAtFirstError: true,
    })

    if (errors.length > 0) {
      console.info('errors[0]', errors[0])
      const errorDetails: {
        type: Paramtype
        property: string
        errorCode: string
      }[] = errors.map((err) => {
        const { property, constraints }: ValidationError = err

        const firstConsraintsKey: string = Object.keys(constraints)[0]
        const keyWithRemoveConsraints = constraints[firstConsraintsKey]
          .replace(property, '')
          .trim()
          .replace(/\s/g, '_')
          .toUpperCase()

        return {
          type,
          property,
          errorCode: keyWithRemoveConsraints,
        }
      })
      // / ส่ง response  ให้ตรงกับ  all-execption.fillter.ts
      throw new HttpException(
        {
          errCode: 'E-1',
          errMessage: 'Not Acceptable',
          errorDetails,
        },
        HttpStatus.NOT_ACCEPTABLE,
      )
    }
    return value
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = this.FUNCTUIN_TYPE
    return !types.includes(metatype)
  }
}
