interface ConfigurationInterface {
  nodeEnv: string
  port: number
  preBuildEnv: string
}

export default ConfigurationInterface
