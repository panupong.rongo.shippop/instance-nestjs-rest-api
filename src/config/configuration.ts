export default () => ({
  nodeEnv: <string>process.env.NODE_ENV || 'development',
  port: <number>parseInt(process.env.PORT, 10) || 3001,
  preBuildEnv: <string>process.env['docker-compose'] || 'NOTHING',
  jwtSecret: <string>process.env.JWT_SECRET || 'CAT',
  jwtExprieIn: <string>process.env.JWT_EXPIRE_IN || '1800s',
})
