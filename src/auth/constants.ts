import Configuration from '../config/configuration'

const { jwtSecret, jwtExprieIn } = Configuration()

export const jwtConstants = {
  secret: jwtSecret,
  expireIn: jwtExprieIn,
}
