import { Strategy } from 'passport-http-bearer'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import { AuthService } from './auth.service'

@Injectable()
export class BearerStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super()
  }

  async validate(bearerToken: string): Promise<any> {
    const user = await this.authService.validateUserBearerToken(bearerToken)
    if (!user) {
      throw new UnauthorizedException()
    }
    return user
  }
}
