import { Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { UsersService } from '../users/users.service'

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUserEmail(email: string, password: string): Promise<any> {
    const user = await this.usersService.findOneByEmail(email)
    if (user && user.password === password) {
      const { id, role } = user
      return { id, email, role }
    }
    return null
  }

  async validateUserBearerToken(bearerToken: string): Promise<any> {
    const user = await this.usersService.findOneByBearerToken(bearerToken)
    if (user) {
      const { id, email, role } = user
      return { id, email, role }
    }
    return null
  }

  async login(user: any) {
    const payload = { email: user.email, sub: user.id, role: user.role }
    return {
      access_token: this.jwtService.sign(payload),
    }
  }
}
